<?php 
class Validation {
	public function sanitize_string ($string) {
		return filter_var($string, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
	}

	public function sanitize_email ($email) {
		return filter_var($email, FILTER_VALIDATE_EMAIL, FILTER_FLAG_STRIP_HIGH);
	}

	private function is_validation_email($email){ 
	    return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
	}

	public function sanitize_date ($date, $format = 'Y-m-d') {
		$d = \DateTime::createFromFormat($format, $date);
	   	return ($d && $d->format($format) === $date) ? $date : '';
	}

	public function sanitize_number ($number) {
		return filter_var($number, FILTER_SANITIZE_NUMBER_INT);
	}

	public function image_validation ($filename) {
		$allowed = ['jpeg', 'png', 'jpg'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		return (in_array($ext, $allowed));
	}

	public function file_size_validation ($file_size, $allowed_size) {
		return ($file_size < $allowed_size);
	}



	public function iFormValidation($request, $files) {
		if (empty($request['name'])) { return 'Name field is required'; }
		if (empty($request['email'])) { return 'Email field is required'; }
		if (empty($request['date_of_birth'])) { return 'Date of birth field is required'; }
		if (empty($request['gender'])) { return 'Gender field is required'; }
		if (empty($request['address'])) { return 'Address field is required'; }
		if (empty($request['employees_in_company'])) { return 'Employee strength field is required'; }
		if (empty($request['hobby'])) { return 'Hobby field is required'; }
		if (empty($request['birth_time'])) { return 'Birth time field is required'; }
		if (!isset($files['profile']['name'])) { return 'Profile field is required'; }

		if (empty($this->sanitize_string($request['name']))) { return 'Name is not valid.'; }
		if (empty($this->is_validation_email($request['email']))) { return 'Email is not valid.'; }
		if (empty($this->sanitize_date($request['date_of_birth']))) { return 'Date of birth is not YYYY-MM-DD formatted date.'; }
		if (empty($this->sanitize_string($request['gender']))) { return 'Gender field is not a valid value'; }
		if (empty($this->sanitize_string($request['address']))) { return 'Address field is not valid.'; }
		if (empty($this->sanitize_number($request['employees_in_company']))) { return 'Employee strength is not a valid number'; }
		if (empty($this->image_validation($files['profile']['name']))) { return 'Profile picture must be a JPEG, JPG, PNG image.'; }
		if (empty($this->file_size_validation($files['profile']['size'], 2000000))) { return 'Profile picture must be less then 2MB'; }

		if ($request['employees_in_company'] < 10 || $request['employees_in_company'] > 500) { return 'Employee strength value must between 10 to 500.'; }
	}

	public function json_response($flag = false, $message = 'Something went wrong!', $data = []) {
		return [
			'FLAG' => $flag,
			'MESSAGE' => $message,
			'DATA' => $data,
		];
	}
}
 ?>
