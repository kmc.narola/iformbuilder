<?php
// Set the header to resolve CORS
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=utf-8');
session_start();

// Include all the required files
include_once 'config.php';
include_once 'APIController.php';
include_once 'Validation.php';

// Initialize the Controller Class
$apicontroller = new APIController;
$helper = new Validation;

// Handle the request comes as POST method
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	// Validate the form
	$validation = $helper->iFormValidation($_REQUEST, $_FILES);

	// If validation failed  then return the validation error message
	if (!empty($validation)) {
		http_response_code(422);
		echo json_encode($helper->json_response(false, "validation fails", ['error' => $validation]));
		exit();
	}

	// Create a uploads directory if not exist
	if ( ! file_exists('./uploads')) { mkdir('./uploads', 0777, true); }
		
	// Upload a profile image, then store the details
	$file_name = rand(9,99999).time().basename($_FILES["profile"]["name"]);
	$path_parts = pathinfo($_FILES["profile"]["name"]);
	$file_extension = $path_parts['extension'];
	$uploads_file = "uploads/$file_name";
	if (move_uploaded_file($_FILES["profile"]["tmp_name"], $uploads_file)) {

		// Sanitize all the requested data
		$data = [
		    "name" => $helper->sanitize_string($_REQUEST['name']),
		    "email" => $helper->sanitize_email($_REQUEST['email']),
		    "date_of_birth" => $helper->sanitize_date($_REQUEST['date_of_birth']),
		    "address" => $helper->sanitize_string($_REQUEST['address']),
		    "employees_in_company" => $helper->sanitize_number($_REQUEST['employees_in_company']),
		    "gender" => $helper->sanitize_string($_REQUEST['gender']),
		    "is_admin" => ($helper->sanitize_number($_REQUEST['is_admin']) == '1' ? 1 : 0),
		    "hobby" => $_REQUEST['hobby'],
		    "birth_time" => $_REQUEST['birth_time'],
		    "profile" => [
		    	"encoding" => "url",
		    	"extension" => $file_extension,
		    	"is_media_internal" => "true",
		    	"value" => str_replace(' ', '', BASE_URL."/".$uploads_file),
		    ],
		];

		// Store the requested data
		$response = $apicontroller->AddFormDetails($data);

		if ($response['status']) {
			// Fetch the stored data by ID
			$form_data = $apicontroller->FetchFormDetails($response['id']);

			if ($form_data['status']) {
				// Send success response
				http_response_code(200);
				echo json_encode($helper->json_response(true, "Data inserted successfully", ['form_data' => $form_data['data']]));
				exit();
			}
		}
	}	

	// Check that any exption in the API and return response as well.
	http_response_code(500);
	echo json_encode($helper->json_response(false, $response['error']));
	exit();
}

// Send 404 response while anyone request using GET method
http_response_code(404);
echo json_encode($helper->json_response(false, 'Request URL does not exist.'));
exit();