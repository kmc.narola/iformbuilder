import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from "./services/api.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { ToastrService } from 'ngx-toastr';
import { oneCheckboxRequired } from "./validators/one-checkbox-required.validator";


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    // Define the variables
    clientForm!: FormGroup;
    maxDate = new Date();
    form_details: any | undefined = [];
    profile_pic: any;

    constructor(private _fb: FormBuilder, 
        private _apiService: ApiService, 
        private ngxService: NgxUiLoaderService, 
        private toastr: ToastrService
    ) {
        this.buildForm();
    }

    ngOnInit(): void {
    }

    // Generate a form with validation
    private buildForm(): void {
        this.clientForm = this._fb.group({
            name: ['', [
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(50)
            ]],
            email: ['', [
                Validators.required,
                Validators.email,
                Validators.minLength(5),
                Validators.maxLength(50)
            ]],
            date_of_birth: ['', [
                Validators.required,
            ]],
            gender: ['male', [
                Validators.required
            ]],
            address: ['', [
                Validators.required,
                Validators.minLength(10),
                Validators.maxLength(100)
            ]],
            employees_in_company: [50, [
                Validators.required,
                Validators.pattern("^[0-9]*$")
            ]],
            is_admin: [false, [
                Validators.required
            ]],
            birth_time: ['', [
                Validators.required
            ]],
            hobby: this._fb.group({
                cricket: [false],
                movie: [false],
                writing: [false],
                reading: [false],
            }, {
                validators: oneCheckboxRequired()
            }),
            profile: [null, [Validators.required]],
        });
    }

    // Handle form submission
    form_submit() {
        // Create a formdata request and add params to that
        let data = new FormData();
        for (let key in this.clientForm.value) {
            if (key === 'date_of_birth') {
                let date_of_birth = new Date(this.clientForm.get('date_of_birth')?.value);
                data.append(key, date_of_birth.toISOString().split('T')[0]);
            }
            else if (key === 'birth_time') {
                let birth_time = new Date(this.clientForm.get('birth_time')?.value);
                let formatted_birth_time = ("0" + birth_time.getHours()).slice(-2) + ":" + ("0" + birth_time.getMinutes()).slice(-2) + ":00";
                data.append(key, formatted_birth_time);
            }
            else if (key === 'is_admin') {
                data.append(key, this.clientForm.get('is_admin')?.value ? '1' : '0');
            }
            else if (key === 'profile') {
                data.append('profile', this.profile_pic);
            }
            else if (key === 'hobby') {
                var hobby_object = this.clientForm.get('hobby')?.value;
                var hobby_string = '';
                Object.keys(hobby_object).forEach(function (key, index) {
                    if (hobby_object[key]) {
                        hobby_string += (hobby_string.length) ? (',' + key) : key
                    }
                });
                data.append('hobby', hobby_string);
            }
            else {
                data.append(key, this.clientForm.get(key)?.value);
            }
        }

        // Start loader
        this.ngxService.start();

        // Send request to the server to store the form data
        this._apiService.create(data).subscribe((res: any) => {
            this.ngxService.stop();  // hide loader
            if (res['FLAG']) {
                this.form_details = res['DATA']['form_data'];

                this.toastr.success(res['MESSAGE'], 'Success');
                this.clientForm.reset();
            }
            else {
                this.toastr.success(res['MESSAGE'], 'Error');
            }
        }, (error) => {
            // Handle API exception
            this.ngxService.stop();
            if (error.hasOwnProperty('error')) {
                this.toastr.error(error['error']['DATA']['error'], 'Error');
            }
            else {
                this.toastr.error(error['statusText'], 'Error');
            }
        });
    }

    // For validation for the formcontrol as f variable
    get f(): { [key: string]: AbstractControl } {
        return this.clientForm.controls;
    }

    // Handle the uplodad profile and validate it
    public validateProfile(event: any) {
        this.profile_pic = event.target.files[0];

        if (this.profile_pic != undefined || this.profile_pic != null ) {
            var allowedExtensions = new RegExp("(.*?)\.(jpeg|jpg|png)$")
            if (!(allowedExtensions.test(this.profile_pic.type))) {
                this.clientForm.controls['profile'].setErrors({ 'type': 'jpeg, jpg, png' });
            }
            else if (this.profile_pic.size > 2048000) {
                this.clientForm.controls['profile'].setErrors({ 'size': '2048' });
            }
        }

    }

}
