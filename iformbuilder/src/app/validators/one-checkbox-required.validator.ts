import { FormGroup } from '@angular/forms';

// Create a custom validation for the formgroup to check that at least one checkbox is 
// checked or no for given form group
export const oneCheckboxRequired = (minRequired: Number = 1) => {
    return (formGroup: FormGroup) => {
        let checked = 0;

        // Check the all the checkboxs of the same group
        Object.keys(formGroup.controls).forEach(key => {
            const control = formGroup.controls[key];
            if (control.value === true) {
                checked++;
            }
        });

        // If not checked required checkbox then return the validation error
        if (checked < minRequired) {
            return { required: true }
        }

        return null;
    };
};